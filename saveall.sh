#!/bin/bash

# Emplacements de sauvegarde sur l'ordinateur hôte
backup_dir="/chemin/vers/repertoire_de_sauvegarde"
timestamp=$(date +%Y%m%d%H%M%S)

# Lire les informations de connexion pour le serveur
read -p "Entrez le nom d'utilisateur du serveur : " server_user
read -p "Entrez l'adresse IP du serveur : " server_ip

# Lire les informations de connexion PostgreSQL
read -p "Entrez le nom d'utilisateur PostgreSQL : " postgres_user

# Création du répertoire de sauvegarde
mkdir -p "$backup_dir/$timestamp"

# Sauvegarde de la base de données Mattermost sur le serveur distant
ssh "$server_user@$server_ip" "pg_dump -U $postgres_user nom_base_de_donnees_mattermost" > "$backup_dir/$timestamp/mattermost.sql"

# Sauvegarde de la base de données GED Wiki sur le serveur distant
ssh "$server_user@$server_ip" "pg_dump -U $postgres_user nom_base_de_donnees_gedwiki" > "$backup_dir/$timestamp/gedwiki.sql"

# Copie des fichiers de sauvegarde depuis le serveur distant
scp "$server_user@$server_ip:$backup_dir/$timestamp/mattermost.sql" "$backup_dir/$timestamp/"
scp "$server_user@$server_ip:$backup_dir/$timestamp/gedwiki.sql" "$backup_dir/$timestamp/"

# Création d'une archive sur l'ordinateur hôte
tar -czvf "$backup_dir/backup_$timestamp.tar.gz" "$backup_dir/$timestamp"

# Nettoyage des fichiers temporaires sur l'ordinateur hôte
rm -rf "$backup_dir/$timestamp"

echo "Sauvegarde terminée dans $backup_dir/backup_$timestamp.tar.gz"




#!/bin/bash

# Define the backup directory
BACKUP_DIR="/path/to/backups"

# Create the backup directory if it doesn't already exist
mkdir -p $BACKUP_DIR

# Backup the Mattermost database
mysqldump -u mattermost -p mattermost > $BACKUP_DIR/mattermost.sql

# Backup the GED Wiki database
mysqldump -u gedwiki -p gedwiki > $BACKUP_DIR/gedwiki.sql

# Compress the backup files
gzip $BACKUP_DIR/mattermost.sql
gzip $BACKUP_DIR/gedwiki.sql

# Remove the uncompressed backup files
rm $BACKUP_DIR/mattermost.sql
rm $BACKUP_DIR/gedwiki.sql

# Display a message indicating that the backup was successful
echo "Backup was successful!"