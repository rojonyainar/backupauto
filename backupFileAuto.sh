#!/bin/bash

# Fonction pour envoyer un e-mail en cas d'erreur
send_error_email() {
  local error_message="$1"
  local recipient="mail"
  local subject="Erreur lors de la sauvegarde en local le $(date +"%Y-%m-%d")"
  local attachment="/opt/err.log"

  echo -e "$error_message" | mutt -s "$subject" -a "$attachment" -- "$recipient"
  echo "E-mail d'erreur envoyé avec succès."
}

BACKUP_DIR="/opt/backup"

DATE=$(date +"%Y-%m-%d")
TIME=$(date +"%H:%M:%S")

mkdir -p "$BACKUP_DIR/$DATE"

for DIR in /var/log/nginx /var/log/odoo /var/log/onlyoffice /var/log/auth.log /var/log/postgresql /var/log/user.log /etc/nginx /opt/mattermost/data; do
  SIZE=$(du -sh "$DIR" | cut -f1)

  echo "$TIME : Backing up $DIR ($SIZE)"

  BASEDIR=$(basename "$DIR")
  tar czf "$BACKUP_DIR/$DATE/$SERVER/$BASEDIR.tgz" -C "$(dirname "$DIR")" "$BASEDIR" 2>>err.log

  if [ $? -ne 0 ]; then
    echo "Une erreur s'est produite lors de la sauvegarde de $DIR"
    send_error_email "Une erreur s'est produite lors de la sauvegarde de $DIR en local."
    exit 1
  fi
done

#dump postgres
sudo -i -u postgres pg_dumpall > "$BACKUP_DIR/$DATE/$SERVER/allpostgresdb_dump.sql"

#dump mariadb
#mysqldump -u {utilisateur} -p{mot_de_passe} {nom_de_la_base_de_données} > "$BACKUP_DIR/$DATE/$SERVER/db_mariadb_dump.sql"

if [ -s err.log ]; then
  echo "Une erreur s'est produite pendant la sauvegarde :"
  cat err.log
  send_error_email "Une erreur s'est produite pendant la sauvegarde en local. Les détails sont dans le log."
  exit 1
fi

echo "$TIME : La sauvegarde s'est terminée avec succès"

# Calculer la date d'une semaine à partir de maintenant
ONE_WEEK_AGO=$(date -d "1 week ago" +"%Y-%m-%d")

# Supprimer les sauvegardes datant de plus d'une semaine.
for backup_folder in "$BACKUP_DIR"/*; do
  if [ -d "$backup_folder" ]; then
    backup_date=$(basename "$backup_folder")
    if [[ "$backup_date" < "$ONE_WEEK_AGO" ]]; then
      echo "Suppression des anciennes sauvegardes: $backup_folder"
      rm -r "$backup_folder"
    fi
  fi
done

# scp -r /opt/backup <USER>@<SERVER_IP>:/destination/path/
