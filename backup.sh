#!/bin/bash

# Demander à l'utilisateur où placer la sauvegarde
read -p "Entrez le chemin complet du répertoire de sauvegarde (laissez vide pour le chemin par défaut) : " custom_backup_dir

# Utiliser un chemin par défaut si l'utilisateur n'a rien saisi
if [ -z "$custom_backup_dir" ]; then
    default_backup_dir="/chemin/vers/repertoire_de_sauvegarde_par_defaut"
    backup_dir="$default_backup_dir"
    read -p "La sauvegarde va être enregistrée dans : $default_backup_dir. Appuyez sur Entrée pour continuer ou entrez un autre chemin et appuyez sur Entrée : "
else
    backup_dir="$custom_backup_dir"
fi

# Vérifier si le répertoire de sauvegarde existe, sinon le créer
mkdir -p "$backup_dir"
timestamp=$(date +%Y%m%d%H%M%S)

# Lire les informations de connexion pour le serveur
read -p "Entrez le nom d'utilisateur du serveur : " server_user
read -p "Entrez l'adresse IP du serveur : " server_ip

# Affichage des options de sauvegarde
echo "Options de sauvegarde disponibles :"
echo "1. Sauvegarde des bases de données"
echo "2. Sauvegarde des logs"
echo "3. Sauvegarde d'autres fichiers"
echo "4. Quitter"

# Demander à l'utilisateur de choisir une option
read -p "Entrez le numéro de l'option que vous souhaitez exécuter : " selected_option

case $selected_option in
    1)
        # Vérification de la présence de bases de données et listage
        db_types=()

        if ssh "$server_user@$server_ip" "mysql --version > /dev/null"; then
            db_types+=("mysql")
        fi

        if ssh "$server_user@$server_ip" "psql --version > /dev/null"; then
            db_types+=("postgres")
        fi

        if ssh "$server_user@$server_ip" "echo oracle > /dev/null"; then
            db_types+=("oracle")
        fi

        if [ ${#db_types[@]} -eq 0 ]; then
            echo "Aucune base de données MySQL, PostgreSQL ou Oracle trouvée sur le serveur."
            exit 1
        fi

        echo "J'ai constaté les bases de données suivantes sur le serveur :"
        for db_type in "${db_types[@]}"; do
            echo "- $db_type"
        done

        # Demander quelles bases de données l'utilisateur souhaite sauvegarder
        read -p "Entrez le numéro des bases de données que vous souhaitez sauvegarder (séparés par des espaces) : " selected_dbs

        # Convertir les entrées en tableau
        IFS=" " read -ra selected_dbs_array <<< "$selected_dbs"

        for selected_db in "${selected_dbs_array[@]}"; do
            case "$selected_db" in
                "mysql")
                    # Sauvegarde MySQL
                    ssh "$server_user@$server_ip" "mysqldump -u $db_user -p'$db_password' nom_base_de_donnees" > "$backup_dir/$timestamp/db_backup_$db_type.sql"
                    ;;
                "postgres")
                    # Sauvegarde PostgreSQL
                    ssh "$server_user@$server_ip" "pg_dump -U $db_user nom_base_de_donnees" > "$backup_dir/$timestamp/db_backup_$db_type.sql"
                    ;;
                "oracle")
                    # Sauvegarde Oracle
                    # ...
                    ;;
                *)
                    echo "Base de données non reconnue : $selected_db"
                    ;;
            esac
        done

        ;;
    2)
        # Sauvegarde des logs
        read -p "Entrez le chemin absolu vers le répertoire des logs sur le serveur : " logs_path
        ssh "$server_user@$server_ip" "tar -czvf /tmp/logs_$timestamp.tar.gz $logs_path"
        scp "$server_user@$server_ip:/tmp/logs_$timestamp.tar.gz" "$backup_dir/$timestamp/"
        ;;
    3)
        # Demander si l'utilisateur souhaite sauvegarder d'autres fichiers
        read -p "Voulez-vous sauvegarder d'autres fichiers ? (oui/non) " backup_other_files

        backup_other_files_lower=$(echo "$backup_other_files" | tr '[:upper:]' '[:lower:]') # Convertir en minuscules

        if [ "$backup_other_files_lower" == "oui" ]; then
            read -p "Entrez le chemin absolu du fichier/dossier à sauvegarder : " other_file_path
            scp "$server_user@$server_ip:$other_file_path" "$backup_dir/$timestamp/"
        fi
        ;;
    4)
        echo "Sortie du script."
        exit 0
        ;;
    *)
        echo "Option invalide."
        exit 1
        ;;
esac

# Création d'une archive sur l'ordinateur hôte
tar -czvf "$backup_dir/backup_$timestamp.tar.gz" "$backup_dir/$timestamp"

# Nettoyage des fichiers temporaires sur l'ordinateur hôte
rm -rf "$backup_dir/$timestamp"

echo "Sauvegarde terminée dans $backup_dir/backup_$timestamp.tar.gz"
