#!/bin/bash

# Fonction pour envoyer un e-mail en cas d'erreur
send_error_email() {
  local error_message="$1"
  local recipient="mail"
  local subject="Erreur lors de la sauvegarde en local le $(date +"%Y-%m-%d")"
  local attachment="/opt/err.log"

  echo -e "$error_message" | mutt -s "$subject" -a "$attachment" -- "$recipient"
  echo "E-mail d'erreur envoyé avec succès."
}

BACKUP_DIR="/opt/backupdb"
PG_USER="myuser" # Remplacez par le nom d'utilisateur PostgreSQL
PG_PASSWORD="your_password" # Remplacez par le mot de passe PostgreSQL
MYSQL_USER="mymysqluser" # Remplacez par le nom d'utilisateur MariaDB
MYSQL_PASSWORD="your_mysql_password" # Remplacez par le mot de passe MariaDB

DATE=$(date +"%Y-%m-%d")
TIME=$(date +"%H:%M:%S")

mkdir -p "$BACKUP_DIR/$DATE"

# Utilisation de PGPASSWORD pour fournir le mot de passe à psql
export PGPASSWORD=$PG_PASSWORD

 Sauvegarde des bases de données PostgreSQL
for DB in $(psql -Atc "SELECT datname FROM pg_database WHERE datname != 'postgres'" -U $PG_USER); do
  SIZE=$(du -sh $DB | cut -f1)
  echo "$TIME : Backing up PostgreSQL database $DB ($SIZE)"
  pg_dump -Fc -U $PG_USER $DB > "$BACKUP_DIR/$DATE/$DB.dump"
  
  if [ $? -ne 0 ]; then
    echo "Une erreur s'est produite lors de la sauvegarde de $DB"
    send_error_email "Une erreur s'est produite lors de la sauvegarde de $DB en local."
    exit 1
  fi
done

# Sauvegarde des bases de données PostgreSQL
for DB in $(psql -Atc "SELECT datname FROM pg_database WHERE datname != 'postgres'" -U $PG_USER); do
  SIZE=$(du -sh $DB | cut -f1)
  echo "$TIME : Back up PostgreSQL database $DB ($SIZE)"
  
  if [ -n "$PG_PASSWORD" ]; then
    pg_dump -Fc -U $PG_USER $DB > "$BACKUP_DIR/$DATE/$DB.dump"
  else
    pg_dump -Fc -U $PG_USER -w $DB > "$BACKUP_DIR/$DATE/$DB.dump"
  fi
  
  dump_status=$?  # Récupérer le statut de la commande
  
  if [ $dump_status -ne 0 ]; then
    echo "Une erreur s'est produite lors de la sauvegarde de $DB"
    send_error_email "Une erreur s'est produite lors de la sauvegarde de $DB en local."
    exit 1
  fi
done

# Sauvegarde des bases de données MariaDB
for DB in $(mysql -N -B -u$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES" | grep -Ev "mysql|information_schema|performance_schema|sys"); do
  SIZE=$(du -sh $DB | cut -f1)
  echo "$TIME : Backing up MariaDB database $DB ($SIZE)"
  mysqldump -u$MYSQL_USER -p$MYSQL_PASSWORD --databases $DB > "$BACKUP_DIR/$DATE/$DB.sql"
  
  if [ $? -ne 0 ]; then
    echo "Une erreur s'est produite lors de la sauvegarde de $DB"
    send_error_email "Une erreur s'est produite lors de la sauvegarde de $DB en local."
    exit 1
  fi
done

if [ -s err.log ]; then
  echo "Une erreur s'est produite pendant la sauvegarde :"
  cat err.log
  send_error_email "Une erreur s'est produite pendant la sauvegarde en local. Les détails sont dans le log."
  exit 1
fi

echo "$TIME : La sauvegarde s'est terminée avec succès"
